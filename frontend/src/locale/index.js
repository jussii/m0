/* ============
 * Locale
 * ============
 *
 * For a multi-language application, you can
 * specify the languages you want to use here.
 */

import en from './en';
import fi from './fi';

export default {
  en,
  fi,
};
