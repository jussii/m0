/* ============
 * English Language File
 * ============
 */

 import home from './home.json';
 import navigation from './navigation.json';
 import signup from './signup.json';

 export default {
   home,
   navigation,
   signup,
 };
