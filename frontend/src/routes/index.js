/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

import Participants from '../pages/Participants';
import Signup from '../pages/Signup';
import Edit from '../pages/Edit';
import Contact from '../pages/Contact';
import Admin from '../pages/Admin';
import Login from '../pages/Login';

export default [
  {
    path: '/',
    name: 'signup.index',
    component: Signup,
  },
  {
    path: '/participants',
    name: 'participants.index',
    component: Participants,
  },
  {
    path: '/edit',
    name: 'edit.index',
    component: Edit,
  },
  {
    path: '/contact',
    name: 'contact.index',
    component: Contact,
  },
  {
    path: '/login',
    name: 'login.index',
    component: Login,
  },
  {
    path: '/admin',
    name: 'admin.index',
    component: Admin,
    meta: {
      auth: true,
    },
  },
  {
    path: '/',
    redirect: '/',
  },
  {
    path: '/*',
    redirect: '/',
  },
];
