export default {
    signupStartInvited: new Date(1569402000000),
    signupEndInvited: new Date(1592956740000),
    signupStart: new Date(1583143200000),
    signupEnd: new Date(1592956740000),
    maxParticipants: 200,
}
