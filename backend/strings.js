module.exports = {
  fi: {
    signupCreate: {
      subject: 'Tervetuloa 34. Muistinnollaukseen',
      paragraphs: [
          'Kiitos ilmoittautumisesta ja tervetuloa juhlistamaan 34-vuotiasta Tietokiltaa!',
          'Juhlan Cocktail-tilaisuus järjestetään T-Talolla (Konemiehentie 2, 02150, Espoo) perjantaina 28.02.2020. Cocktail-tilaisuuden jälkeen siirrytään pääjuhlapaikalle Hotelli Presidenttiin bussikuljetuksin klo. 18:00 alkaen.',
          'Pääjuhlan jälkeen on bussikuljetus avoimille jatkoille Smökkiin (Jämeräntaival 4).',
          'Ikuisille jatkoilijoille on mahdollisuus siirtyä Rantsulle klo. 3:30 alkaen.',
          'Seuraavana aamuna onkin sitten aika siirtyä kohti sillistä paikkaan X bussikuljetuksin klo. 11:30, 12:00 ja 12:30 Teekkarikylän päätepysäkiltä (Otakaari 18). Sillikseltä on bussikuljetukset pois klo 17:30, 18:00 ja 18:30',
          'Juhlien pukukoodi on juhlapuku akateemisin kunniamerkein. Luvassa on suuret juhlat, joten varaathan mukaan myös iloista juhlamieltä sekä käteistä.',
          'Juhlan telegram kanavalle voit liittyä linkistä: https://t.me/joinchat/D476ZRFCoYODUS7VaZK_Jw',
          'Ilmoittauduit Muistinnollaukseen seuraavin tiedoin:',
          '{signupDetails}',
          'Voit muokata tietojasi ilmoittautumisen sulkeutumiseen mennessä osoitteessa {modifyLink}',
          'Maksutiedot löydät alempaa.',
          'Saaja: Tietokilta ry\nTili: FI44 3131 3001 5440 71\nSwift/BIC: HANDFIHH\nSumma: {signupPrice}€\nEräpäivä: 22.02.2020\nViesti: m0, "oma nimi"',
          'Nähdään juhlassa!\n',
          'Ystävällisin terveisin,\n\nJussi Impiö & Lasse Liimatta\n\nMuistinnollaustirehtöörit\n'
      ]
    },
    signupUpdate: {
      prepend: ['Tietosi päivitettiin. Tässä uudet ilmoittautumistietosi']
    },
    signupUpdateReserve: {
      prepend: ['Tietosi päivitettiin']
    },
    signupCreateReserve: {
      subject: 'Muistinnollaus 100010 Varasija',
      paragraphs: [
        'Hei',
        'Kiitos ilmoittautumisesta Muistinnollaukseen. Olet tällä hetkellä varasijalla ja ilmoitamme viimeistään ilmoittautumisen päätyttyä, mikäli juhlassa on tilaa. Seuraathan sähköpostiasi!',
        'Voit muokata tietojasi ilmoittautumisen sulkeutumiseen mennessä osoitteessa {modifyLink}',
        'Mikäli juhliin liittyen on kysyttävää, olethan yhteydessä allekirjoittaneeseen.\n',
        'Ystävällisin terveisin,\n\nJussi Impiö & Lasse Liimatta\n\nMuistinnollaustirehtöörit\n'
      ],
    },
    signupDetailsTemplate: [
      'Nimi: {name}',
      'Sähköposti: {email}',
      'Opiskelujen aloitusvuosi: {start_year}',
      'Olen opiskelija: {student}',
      'Avecin nimi: {avec}',
      'Ruoka-aineallergiat sekä erityisruokavaliot: {food_requirements}',
      'Pöytäseurue: {table_group}',
      'Alkoholiton: {no_alcohol}',
      'Edustamani taho: {representative_of}',
      'Ilmoittaudun sillikselle 29.02.2020 (+20€): {sillis}',
      'Haluan antaa lahjan: {present}',
    ],
    booleans: ['Kyllä', 'Ei']
  },
  en: {
    signupCreate: {
      subject: 'Tervetuloa 34. Muistinnollaukseen',
      paragraphs: [
        'Kiitos ilmoittautumisesta ja tervetuloa juhlistamaan 34-vuotiasta Tietokiltaa!',
        'Juhlan Cocktail-tilaisuus järjestetään T-Talolla (Konemiehentie 2, 02150, Espoo) perjantaina 28.02.2020. Cocktail-tilaisuuden jälkeen siirrytään pääjuhlapaikalle Hotelli Presidenttiin bussikuljetuksin klo. 18:00 alkaen.',
        'Pääjuhlan jälkeen on bussikuljetus avoimille jatkoille Smökkiin (Jämeräntaival 4).',
        'Ikuisille jatkoilijoille on mahdollisuus siirtyä Rantsulle klo. 3:30 alkaen.',
        'Seuraavana aamuna onkin sitten aika siirtyä kohti sillistä paikkaan X bussikuljetuksin klo. 11:30, 12:00 ja 12:30 Teekkarikylän päätepysäkiltä (Otakaari 18). Sillikseltä on bussikuljetukset pois klo 17:30, 18:00 ja 18:30',
        'Juhlien pukukoodi on juhlapuku akateemisin kunniamerkein. Luvassa on suuret juhlat, joten varaathan mukaan myös iloista juhlamieltä sekä käteistä.',
        'Juhlan telegram kanavalle voit liittyä linkistä: https://t.me/joinchat/D476ZRFCoYODUS7VaZK_Jw',
        'Ilmoittauduit Muistinnollaukseen seuraavin tiedoin:',
        '{signupDetails}',
        'Voit muokata tietojasi ilmoittautumisen sulkeutumiseen mennessä osoitteessa {modifyLink}',
        'Maksutiedot löydät alempaa.',
        'Saaja: Tietokilta ry\nTili: FI44 3131 3001 5440 71\nSwift/BIC: HANDFIHH\nSumma: {signupPrice}€\nEräpäivä: 22.02.2020\nViesti: m0, "oma nimi"',
        'Nähdään juhlassa!\n',
        'Ystävällisin terveisin,\n\nJussi Impiö & Lasse Liimatta\n\nMuistinnollaustirehtöörit\n'
      ]
    },
    signupUpdate: {
      prepend: ['Your info has been updated. Here are your new registration details.']
    },
    signupUpdateReserve: {
      prepend: ['Your info has been updated']
    },
    signupCreateReserve: {
      subject: 'Muistinnollaus 100010 Varasija',
      paragraphs: [
        'Hei',
        'Kiitos ilmoittautumisesta Muistinnollaukseen. Olet tällä hetkellä varasijalla ja ilmoitamme viimeistään ilmoittautumisen päätyttyä, mikäli juhlassa on tilaa. Seuraathan sähköpostiasi!',
        'Voit muokata tietojasi ilmoittautumisen sulkeutumiseen mennessä osoitteessa {modifyLink}',
        'Mikäli juhliin liittyen on kysyttävää, olethan yhteydessä allekirjoittaneeseen.\n',
        'Ystävällisin terveisin,\n\nJussi Impiö & Lasse Liimatta\n\nMuistinnollaustirehtöörit\n'
      ],
    },
    signupDetailsTemplate: [
      'Name: {name}',
      'Email: {email}',
      'Freshman year: {start_year}',
      'I am a student: {student}',
      'Name of avec: {avec}',
      'Menu options: {dish}',
      'Food allergies and special diet: {food_requirements}',
      'Non-alcoholic: {no_alcohol}',
      'Seating preferences: {table_group}',
      'Representative of: {representative_of}',
      'I participate in sillis brunch on 29.02. (+25€): {sillis}',
      'I\'ll give a present: {present}',
    ],
    booleans: ['Yes', 'No']
  },
};